<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::match(['get','post'],'/','\App\Http\Controllers\urlController@index')->name('index');
Route::post('/create-url', '\App\Http\Controllers\urlController@store')->name('store');
Route::get('/shorted/{link}','\App\Http\Controllers\urlController@traffic')->name('traffic');
Route::view('/403', '403');
