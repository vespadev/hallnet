<?php

namespace App\Http\Controllers;

use App\Models\traffic;
use App\Models\urls;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class UrlController extends Controller
{
    private $shortList = array();

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View|Response
     */
    public function index()
    {
        try {
            $urls = DB::table('urls')
                ->where('private', 0)
                ->leftjoin('traffic', 'traffic.short_id', '=', 'urls.id')
                ->select(DB::raw('urls.*, counting'))
                ->orderByDesc('created_at')
                ->take(10)
                ->get();
            return view(
                'welcome', [
                    'urls' => $urls,
                ]
            );

        } catch (\Exception $urls) {
            return redirect('/403');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $url = new urls();
        $this->getShortWordlist();
        $shortURL = (!$request->input('short_url') ? $this->generateShortUrl() : $this->clean($request->input('short_url')));
        $error = $this->checkShortExist($shortURL);
        if ($error == 0) {
            $url->long_url = $request->input('long_url');
            $url->short_url = $shortURL;
            $url->private = ($request->input('type_url') == 1 ? true : false);
            $url->save();
            $generate = \Request::root() .'/'. $shortURL;
            return redirect()->back()->with('message', "This shorted url has been added: <span>".$generate."</span>");
        } else
            return redirect()->back()->with('message', "This shorted url can't be added");

    }

    /**
     * Clean string for special chars.
     *
     * @param $text
     * @return string
     */
    private function clean($text): string
    {
        $string = str_replace(' ', '-', $text);
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);

        return preg_replace('/-+/', '-', $string);
    }

    /**
     * Generate a random number for short url.
     *
     * @return string
     */
    private function generateShortUrl(): string
    {
        $compile = '';
        do {
            $error = 0;
            $new_url = rand(1111, 6666);
            if (isset($this->shortList[$new_url]))
                $compile = $this->shortList[$new_url];
            else
                $error = 1;
        } while ($error == 1);
        return $compile;
    }

    /**
     * Parsing url to create array with all short urls.
     *
     * @return RedirectResponse
     */
    private function getShortWordlist()
    {
        try {
            $response = file_get_contents('https://www.eff.org/files/2016/09/08/eff_short_wordlist_2_0.txt');
            $array = preg_split('/\n/', $response);
            foreach ($array as $elem) {
                $splitted = preg_split('/\t/', $elem);
                if ($splitted[0] != null)
                    $this->shortList[$splitted[0]] = $splitted[1];
            }
        } catch (Exception $exception) {
            return redirect('/403');
        }
    }

    /**
     * Checking if short url already exists.
     *
     * @return bool
     */
    private function checkShortExist($val): bool
    {
        if (urls::where('short_url', '=', $val)->exists()) return 1;
        else return 0;
    }

    /**
     * Getting long url and redirect to it.
     *
     * @return RedirectResponse
     */
    public function traffic($link)
    {
        $main_link = urls::where('short_url', '=', $link)->first();
        $check = DB::table('traffic')
            ->where('short_id', $main_link->id)
            ->exists();
        if ($check == false)
            $this->createTraffic($main_link->id);
        else
            $this->incrementTraffic($main_link->id);
        return redirect($main_link->long_url);
    }

    private function createTraffic($link): void
    {
        $traffic = new traffic();
        $traffic->short_id = $link;
        $traffic->counting = 1;
        $traffic->save();
    }

    private function incrementTraffic($link): void
    {
        DB::table('traffic')
            ->where('traffic.short_id', '=', $link)
            ->increment('counting');
    }
}
