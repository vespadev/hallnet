<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class urls extends Model
{
    use HasFactory;

    /**
     * @var mixed
     */
    protected $table = 'urls', $primaryKey = 'id';
    protected $fillable = [
        'long_url',
        'short_url',
        'private'
        ];

}
