<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>A Web Page</title>
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body>
<div class="container">
    <img src="{{ asset('img/hn-bit-logo.png') }}" class="header-img" alt="logo">
</div>
@if (\Session::has('message'))
    <div class="message">
        <h4>{!! \Session::get('message') !!}</h4>
    </div>
@endif
<div class="main-container">
    <div class="container">
        <form method="post" action="{{ url('/create-url')}}">
            @csrf
            <input type="url" name="long_url" placeholder="Long URL (required)" required>
            <input type="text" name="short_url" placeholder="Short URL keyword (optional)">
            <div class="checkbox-custom">
                <input type="checkbox" name="type_url" value="1">
                <label> Private?</label><br>
            </div>
            <input type="submit" value="Shorten" class="btn btn-info">
        </form>
        <h1>Recent links</h1>
        <ul class="custom-list">
            @foreach($urls as $url)
                @if (isset($url))
                    <li>
                        <a href="{{ url('/shorted/'.$url->short_url) }}">{{\Request::root().'/' .$url->short_url}}</a> -
                        <label>
                            {{$url->counting ? $url->counting : '0'}} views </label>
                        <p>{{$url->created_at}}</p>
                        <p>{{$url->long_url}}</p>
                    </li>
                @endif
            @endforeach
        </ul>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script type="text/javascript" src="{{URL::asset('js/app.js')}}"></script>
</body>
</html>
